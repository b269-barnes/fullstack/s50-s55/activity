import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';

import {Container} from 'react-bootstrap'

import './App.css';


function App() {
  return (
    // <></> fragments - common pattern in React for component to return multiple elements
    <>
      <AppNavBar/>
      <Container>
      <Home/>
      </Container>

    </>
  );
}

export default App;
