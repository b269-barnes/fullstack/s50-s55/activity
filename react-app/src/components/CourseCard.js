import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

function BasicExample() {
  return (
    <Card style={{ width: '100%' }} className="cardHighlight mb-3 ">

      <Card.Body>
        <Card.Title >Sample Course</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>
          This is a sample course offering.
        </Card.Text>
        <Card.Subtitle >Price:</Card.Subtitle>
        <Card.Subtitle >
          Php 40,000
        </Card.Subtitle>
        <Card.Text>  </Card.Text>
        <Button variant="primary">Enroll</Button>
      </Card.Body>
    </Card>
  );
}

export default BasicExample;